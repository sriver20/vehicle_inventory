Syncroyness Example Project
===========================

Author: Salvador Rivera Jr

This is an simple example of a project that I created from scratch. With more time I could
improve the gui, possibly with javaFX, and create unit test using using hamcrest and junit.

Unittest crossing packages is useful for integration testing.

Git Config
----------
I did not add a .gitignore to this project. I would ignore the bin and dist, directories.

Requirements
------------
java version "1.8.0_20"
Apache Ant(TM) version 1.9.3 compiled on December 23 2013


Directory Map
-------------

```
├── bin
│   ├── Bmw.class
│   ├── Ford.class
│   ├── Inventory.class
│   ├── Manufacturer.class
│   ├── Tesla.class
│   └── Vehicle.class
├── build.xml
├── dist
│   └── Inventory-20180211.jar
├── lib
│   ├── hamcrest-core-1.3.jar
│   └── junit-4.6.jar
├── README.md
└── src
    ├── Bmw.java
    ├── Ford.java
    ├── Inventory.java
    ├── Manufacturer.java
    ├── Tesla.java
    └── Vehicle.java
```
