public class Tesla extends Manufacturer {
    private String name = "Tesla";
    private String location = "Fremont, California";

    @Override
    public String toString() {
        return this.name + " " + this.location + " (Batteries Included!)";
    }

    @Override
    public String getName() {
        return this.name;
    }
}
