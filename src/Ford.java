public class Ford extends Manufacturer {
    private String name = "Ford";
    private String location = "Dearborn, Detroit";

    @Override
    public String toString() {
        return this.name + " " + this.location;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
