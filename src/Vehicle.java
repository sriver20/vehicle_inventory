public class Vehicle {
    String vin;
    Manufacturer manufacture;
    String model;
    Integer year;
    String color;

    public Vehicle(String vin, Manufacturer manufacture, String model, Integer year,
                   String color) {
        this.vin = vin;
        this.manufacture = manufacture;
        this.model = model;
        this.year = year;
        this.color = color;
    }

    public Manufacturer getManufacture() {
        return this.manufacture;
    }

    public Integer getYear() {
        return this.year;
    }


    public String getVin() {
        return this.vin;
    }

    @Override
    public String toString() {
        return "VIN: " + this.vin
               + "\nManufacturer: " + this.manufacture
               + "\nModel: " + this.model
               + "\nYear: " + this.year
               + "\nColor: " + this.color + "\n";
    }

}
