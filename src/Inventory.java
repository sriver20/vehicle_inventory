import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.NumberFormatException;
import java.util.ArrayList;
import java.util.HashMap;

public class Inventory {

    // I left everything public, so it can be used in main
    public HashMap<String, Vehicle> vehicles;

    public Inventory() {
        vehicles = new HashMap<String, Vehicle>();
    }

    public void printInventory() {
        this.vehicles.forEach((k, v) -> System.out.println(v));
    }

    public void yearSearch(Integer year) {
        this.vehicles.forEach((k, v) -> {
            if(v.getYear() == year) {
                System.out.println(v);
            }
        });
    }

    public void manufacturerSearch(String name) {
        this.vehicles.forEach((k, v) -> {
            if(v.getManufacture().getName() == name) {
                System.out.println(v);
            }
        });
    }

    public Vehicle addVehicle() {
        System.out.println("Adding a vehicle");
        String vin = getVehicleVin();
        Manufacturer manufactuer = getManufacture();
        String model = getModel();
        Integer year = getYear();
        String color = getColor();

        Vehicle vehicle = new Vehicle(vin, manufactuer, model, year, color);
        this.vehicles.put(vin, vehicle);
        return vehicle;
    }

    public Vehicle removeVehicle(String vin) {
        System.out.println("Removing a vehicle");
        Vehicle vehicle = this.vehicles.remove(vin);
        if(vehicle == null) {
            System.out.println("did not find a vehicle with that vin");
        }
        return vehicle;
    }


    public Vehicle removeVehicle() {
        System.out.println("Removing a vehicle");
        String vin = getVehicleVin();
        Vehicle vehicle = this.vehicles.remove(vin);
        if(vehicle == null) {
            System.out.println("did not find a vehicle with that vin");
        }
        return vehicle;
    }

    public String getVehicleVin() {
        System.out.println("Please enter the vehicle's vin:");
        String vin = getUserInput();
        // Should be checking for valid date, did not right now.
        return vin;
    }

    public Manufacturer getManufacture() {
        System.out.println("Please enter the manufactuer's name");
        Manufacturer manufacturer = null;
        String stringManufacturer;
        while (manufacturer == null) {
            // Should be checking for valid date, did not right now.
            stringManufacturer = getUserInput();
            switch (stringManufacturer) {
                case "BMW":
                    manufacturer = new Bmw();
                    break;
                case "TESLA":
                    manufacturer = new Tesla();
                    break;
                case "FORD":
                    manufacturer = new Ford();
                    break;
                default:
                    System.out.println("Please select bmw, tesla, or ford");
                    break;
            }
        }
        return manufacturer;
    }

    public String getModel() {
        System.out.println("Please enter the Model name");
        String model = getUserInput();
        // Should be checking for valid date, did not right now.
        return model;
    }

    public Integer getYear() {
        System.out.println("Please enter the year manufactured.");
        String stringYear;
        Integer year = 0;
        while (year == 0) {
            stringYear = getUserInput();
            try {
                // Should be checking for valid range, did not right now.
                year = Integer.parseInt(stringYear);
            } catch (Exception e) {
                System.out.println("Please enter valid number.");
            }
        }
        return year;
    }

    public String getColor() {
        System.out.println("Please enter the vehicle's color");
        String color = getUserInput();
        // Should be checking for valid colors, did not right now.
        return color;
    }

    public String getUserInput(){
        BufferedReader sysIn = new BufferedReader(new InputStreamReader(System.in));
        String userInput = "";
        while (userInput.isEmpty()) {
            try {
                userInput = sysIn.readLine().toUpperCase();
                //sysIn.close();
            } catch (IOException e) {
                System.out.println("Was not able to get input, please try again.");
            }
        }
        return userInput;
    }

    public static void main(String args []) {
        System.out.println("Manufacturers");
        System.out.println("=============");
        Manufacturer ford = new Ford();
        Manufacturer tesla = new Tesla();
        Manufacturer bmw = new Bmw();
        System.out.println(ford);
        System.out.println(tesla);
        System.out.println(bmw);
        System.out.println("");

        Integer commomYear = 1979;
        Vehicle ford1 = new Vehicle("ABC123", ford, "F-150", commomYear, "BLUE");
        Vehicle ford2 = new Vehicle("ABC456", ford, "RANGER", 1990, "BLACK");

        Vehicle tesla1 = new Vehicle("DEF123", tesla, "HAHA", commomYear, "SUNSET");
        Vehicle tesla2 = new Vehicle("DEF456", tesla, "JAJA", 2014, "SUNSET");

        Vehicle bmw1 = new Vehicle("GHI123", bmw, "300", commomYear, "WHITE");
        Vehicle bmw2 = new Vehicle("GHI456", bmw, "500", 2016, "YELLOW");

        Inventory inventory = new Inventory();
        inventory.vehicles.put(ford1.getVin(), ford1);
        inventory.vehicles.put(ford2.getVin(), ford2);

        inventory.vehicles.put(tesla1.getVin(), tesla1);
        inventory.vehicles.put(tesla2.getVin(), tesla2);

        inventory.vehicles.put(bmw1.getVin(), bmw1);
        inventory.vehicles.put(bmw2.getVin(), bmw2);

        System.out.println("Print all Inital Inventory");
        System.out.println("==========================");
        inventory.printInventory();

        System.out.println("Print all Inital Inventory with common year");
        System.out.println("===========================================");
        inventory.yearSearch(commomYear);

        System.out.println("Print all Inital Inventory made by Ford");
        System.out.println("=======================================");
        inventory.manufacturerSearch("Ford");

        System.out.println("Clearing all inventory.");
        System.out.println("=======================");
        inventory.removeVehicle(ford1.getVin());
        inventory.removeVehicle(ford2.getVin());
        inventory.removeVehicle(tesla1.getVin());
        inventory.removeVehicle(tesla2.getVin());
        inventory.removeVehicle(bmw1.getVin());
        inventory.removeVehicle(bmw2.getVin());
        System.out.println("Printing Inventory after all vehicles were removed");
        System.out.println("==================================================");
        inventory.printInventory();

        inventory.addVehicle();
        System.out.println("Printing Inventory after vehicle was added.");
        System.out.println("===========================================");
        inventory.printInventory();

        inventory.removeVehicle();
        System.out.println("Printing Inventory after attempting to remove a vehicle");
        System.out.println("=======================================================");
        inventory.printInventory();
    }
}
