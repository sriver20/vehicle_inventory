public class Bmw extends Manufacturer {
    private String name = "BMW";
    private String location = "Munich, Germany";

    @Override
    public String toString() {
        return this.name + " " + this.location;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
